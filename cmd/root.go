package cmd

import (
	"database/sql"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "epcg",
	Short: "An API server for generating Eclipse Phase characters",
	Long: `An API server for generating Eclipse Phase characters.

Note: Options set via CLI flags will override those specified in configuration file.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(
		&cfgFile,
		"config",
		"",
		"config file (default is /etc/epcg/epcg.yaml)",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDataPath,
		"fate-data-path",
		"",
		"Path to the directory which contains the FATE CSV files with which to initialize or update the database",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDBhost,
		"fate-db-host",
		"",
		"The URL of the database server to connect to for the FATE API",
	)

	rootCmd.PersistentFlags().IntVar(
		&fateDBport,
		"fate-db-port",
		0,
		"The port to connect to on the database for the FATE API",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDBname,
		"fate-db-name",
		"",
		"The name of the database for the FATE API",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDBuser,
		"fate-db-user",
		"",
		"The database user to connect as for the FATE API",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDBpass,
		"fate-db-pass",
		"",
		"The password for the database user for the FATE API",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDBtz,
		"fate-db-tz",
		"",
		"The timezone of the database server for the FATE API",
	)

	rootCmd.PersistentFlags().IntVar(
		&fateRestPort,
		"fate-rest-port",
		0,
		"The port on which to expose the FATE API",
	)

	rootCmd.PersistentFlags().StringVar(
		&fateDBssl,
		"fate-db-ssl",
		"",
		"SSL config for connecting to the database for the FATE API; default: 'disable'",
	)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		// home, err := os.UserHomeDir()
		// cobra.CheckErr(err)

		// Search config in /etc/epcg directory with name "epcg" (without extension).
		viper.AddConfigPath("/etc/epcg")
		viper.AddConfigPath("./run-local")
		viper.SetConfigType("yaml")
		viper.SetConfigName("epcg")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}

	if viper.GetStringMap("fateAPI") != nil {
		fateAPImap = viper.GetStringMap("fateAPI")
	}

	setFateVars()
}

// Sets the FATE API variables based on input, config, or default
func setFateVars() {
	// Set FATE API variables from CLI switches first if defined, then from config file if defined,
	// then finally to the default value

	if fateDBhost != "" {
		fateAPImap["db_host"] = fateDBhost
	} else if fateAPImap["db_host"] == "" {
		fateAPImap["db_host"] = "localhost"
	}
	if fateAPImap["db_host"] == nil {
		fateAPImap["db_host"] = "localhost"
	}

	if fateDBport != 0 {
		fateAPImap["db_port"] = fateDBport
	} else if fateAPImap["db_port"] == 0 {
		fateAPImap["db_port"] = 5432
	}
	if fateAPImap["db_port"] == nil {
		fateAPImap["db_port"] = 5432
	}

	if fateDBname != "" {
		fateAPImap["db_name"] = fateDBname
	} else if fateAPImap["db_name"] == "" {
		fateAPImap["db_name"] = "epcg_fate"
	}
	if fateAPImap["db_name"] == nil {
		fateAPImap["db_name"] = "epcg_fate"
	}

	if fateDBuser != "" {
		fateAPImap["db_user"] = fateDBuser
	} else if fateAPImap["db_user"] == "" {
		fateAPImap["db_user"] = "epcg"
	}
	if fateAPImap["db_user"] == nil {
		fateAPImap["db_user"] = "epcg"
	}

	if fateDBpass != "" {
		fateAPImap["db_pass"] = fateDBpass
	} else if fateAPImap["db_pass"] == "" {
		fateAPImap["db_pass"] = "password"
	}
	if fateAPImap["db_pass"] == nil {
		fateAPImap["db_pass"] = "password"
	}

	if fateDBtz != "" {
		fateAPImap["db_tz"] = fateDBtz
	} else if fateAPImap["db_tz"] == "" {
		fateAPImap["db_tz"] = "US/Eastern"
	}
	if fateAPImap["db_tz"] == nil {
		fateAPImap["db_tz"] = "US/Eastern"
	}

	if fateRestPort != 0 {
		fateAPImap["rest_port"] = fateRestPort
	} else if fateAPImap["rest_port"] == 0 {
		fateAPImap["rest_port"] = 10000
	}
	if fateAPImap["rest_port"] == nil {
		fateAPImap["rest_port"] = 10000
	}

	if fateDataPath != "" {
		fateAPImap["data_path"] = fateDataPath
	} else if fateAPImap["data_path"] == "" {
		fateAPImap["data_path"] = "/etc/epcg/data/FATE"
	}
	if fateAPImap["data_path"] == nil {
		fateAPImap["data_path"] = "/etc/epcg/data/FATE"
	}

	if fateDBssl != "" {
		fateAPImap["db_ssl"] = fateDBssl
	} else if fateAPImap["db_ssl"] == "" {
		fateAPImap["db_ssl"] = "disable"
	}
	if fateAPImap["db_ssl"] == nil {
		fateAPImap["db_ssl"] = "disable"
	}

	nilCheck()

	fatePsqlInfo = fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s TimeZone=%s",
		fateAPImap["db_host"].(string), fateAPImap["db_port"].(int), fateAPImap["db_user"].(string),
		fateAPImap["db_pass"].(string), fateAPImap["db_name"].(string), fateAPImap["db_ssl"].(string),
		fateAPImap["db_tz"].(string),
	)
}

func nilCheck() {
	var maps = []string{"db_host", "db_port", "db_user", "db_pass", "db_name", "db_ssl", "db_tz"}

	for _, m := range maps {
		if fateAPImap[m] == nil {
			log.Fatal("%s is nil!", m)
		}
	}
}

// Contains tells whether a contains x.
func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func ConnectDatabase(psqlInfo string) *sql.DB {
	log.Println("Connecting to database")
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal("DB connection error: ", err)
	}

	log.Println("Testing database connection")
	err = db.Ping()
	if err != nil {
		log.Fatal("DB connection test error: ", err)
	}
	log.Println("Successfully connected to database")

	return db
}

func GetDBfiles(db_files_path string) []string {
	// Slice of the full paths to the CSV files
	var db_csv_files []string

	files, err := ioutil.ReadDir(db_files_path)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Getting CSV files to ingest from: ", db_files_path)
	for i := range files {
		if strings.HasSuffix(files[i].Name(), ".csv") {
			log.Println("Found CSV file: ", files[i].Name())
			db_csv_files = append(db_csv_files, db_files_path+"/"+files[i].Name())
		}
	}

	return db_csv_files
}
