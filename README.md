# Eclipse Phase Character Generator

[![pipeline status](https://gitlab.com/cypher_zero/epcg/badges/main/pipeline.svg)](https://gitlab.com/cypher_zero/epcg/-/commits/main)

This project is a Go based web application to generate Eclipse Phase characters and NPCs.
This is a community supported, non-commercial project.

## Contributing

The primary repository for this codebase resides in GitLab here: <https://gitlab.com/cypher_zero/epcg>

Submissions, issues, pull/merge requests to any other repository (such as GitHub) will *not* be accepted.

## Versions and planned releases

Please see the [Version Plan](./Version_Plan.md) document for more information on the planned versions and releases.

## Licenses

This project is licensed under [GPL v3](./LICENSE.txt) except where other licenses apply.

[Eclipse Phase](https://eclipsephase.com) content is licensed under [Creative Commons Attribution-Noncommercial-Share Alike 3.0 License](https://eclipsephase.com/cclicense).

If you believe that a license has been violated, please [submit an issue via GitLab](https://gitlab.com/cypher_zero/epcg/-/issues).
