#!/bin/bash

HOST_IP="$(ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}')"

docker run --name epcg-api -it -p 10000:10000 \
  --add-host=host.docker.internal:"${HOST_IP}" \
  epcg "epcg start fate \
    --fate-db-host '${HOST_IP}' \
    --fate-db-port 5432 \
    --fate-db-user epcg \
    --fate-db-pass password \
    --fate-db-tz 'US/Eastern' \
    --fate-db-name epcg_fate \
    --fate-rest-port 10000 \
    --db-init-fate"
