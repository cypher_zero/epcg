#!/bin/bash

set -e

CONTAINERS=(
  "epcg-api"
  "epcg-postgres"
)

for container in "${CONTAINERS[@]}"; do
  if docker ps | grep -q "${container}"; then
    echo "Stopping container ${container}"
    docker stop "${container}"
  fi

  if docker ps -a | grep -q "${container}"; then
    echo "Deleting container ${container}"
    docker rm "${container}"
  fi
done
