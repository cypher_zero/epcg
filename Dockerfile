FROM alpine

RUN apk update --no-cache && \
    apk upgrade --no-cache

RUN apk update --no-cache && \
    apk upgrade --no-cache && \
    apk add --no-cache libc6-compat

ADD ./config/default.yaml /etc/epcg/epcg.yaml
ADD ./data/ /var/lib/epcg/data

RUN addgroup -S epcg && \
    adduser  -S -g "epcg" epcg epcg

RUN chown -R epcg:epcg /etc/epcg && \
    chown -R epcg:epcg /var/lib/epcg

ADD ./epcg /bin/epcg

RUN chown root:root /bin/epcg && \
    chmod 755 /bin/epcg

USER epcg

ENTRYPOINT [ "/bin/sh", "-c" ]
CMD [ "/bin/epcg", "start", "all", "--db-init-all" ]
