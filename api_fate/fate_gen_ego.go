package api_fate

import (
	"reflect"
	"strings"

	rand "gitlab.com/cypher_zero/epcg/rnd"
)

// Randomly select faction based on background
func genFactionFromBackground(bkg Background) string {
	// reflection of all elements in the background
	var reflect_bkg reflect.Value = reflect.ValueOf(&bkg).Elem()
	// Map of the faction weights in the background
	var weights_map map[string]int = make(map[string]int)
	// The name of the randomly selected morph to be returned
	var faction_name string

	for i := 0; i < reflect_bkg.NumField(); i++ {
		var_name := reflect_bkg.Type().Field(i).Name
		var_type := reflect_bkg.Type().Field(i).Type

		if strings.Contains(var_name, "_Faction_Weight") && var_type.Name() == "int" {
			var_value := reflect_bkg.Field(i).Interface().(int)
			weights_map[var_name] = var_value
		}
	}

	faction_name = calcFromWeights(weights_map)
	faction_name = strings.Replace(faction_name, "_Faction_Weight", "", 1)

	return faction_name
}

// Generate character skills based on team role
func genSkills(role Team_Role, morph_type string) map[string]int {
	// Output map of the list of skills and their ranks
	char_skills := make(map[string]int)
	// List of all skills
	skill_list := getSkillsList(morph_type)
	// Whether skill pyramid is deep focus (true) or broad focus (false)
	var deep_pyramid_bool bool
	// Slice of primary skill names from role
	var key_skills = []string{role.Key_Skill_1, role.Key_Skill_2, role.Key_Skill_3}

	// The nubmer of average skills to generate
	var avg_skills int

	rnd_skill_pyramid := rand.Intn(2)
	// log.Println("rnd_skill_pyramid:", rnd_skill_pyramid)
	if rnd_skill_pyramid == 0 {
		deep_pyramid_bool = true
	} else {
		deep_pyramid_bool = false
	}

	char_skills[role.Peak_Skill] = 4
	// log.Println(role.Peak_Skill)

	// Get a skill from the key skills
	rnd_skill := rand.Intn(len(key_skills))
	if deep_pyramid_bool {
		char_skills[key_skills[rnd_skill]] = 4
	} else {
		char_skills[key_skills[rnd_skill]] = 3
	}

	// Remove the skill which was just set from the list of key skills
	key_skills[rnd_skill] = key_skills[len(key_skills)-1]
	key_skills = key_skills[:len(key_skills)-1]

	rnd_skill = rand.Intn(len(key_skills))
	char_skills[key_skills[rnd_skill]] = 3

	// Remove the skill which was just set from the list of key skills
	key_skills[rnd_skill] = key_skills[len(key_skills)-1]
	key_skills = key_skills[:len(key_skills)-1]

	if deep_pyramid_bool {
		char_skills[key_skills[0]] = 3
	} else {
		char_skills[key_skills[0]] = 2
	}

	for i := 0; i < 3; i++ {
		skill_name := pickRandSkill(char_skills, skill_list)
		char_skills[skill_name] = 2
	}

	if deep_pyramid_bool {
		avg_skills = 5
	} else {
		avg_skills = 7
	}

	for i := 0; i < avg_skills; i++ {
		skill_name := pickRandSkill(char_skills, skill_list)
		char_skills[skill_name] = 1
	}

	return char_skills
}

// Pick a random skill from a list of skills, ensuring no duplicates
func pickRandSkill(char_skills map[string]int, skill_list []string) string {
	// Check var for if a valid skill has been selected yet
	var chk_bool bool = false
	// Random number used to pick a skill from the list
	var rnd_int int

	for !chk_bool {
		rnd_int = rand.Intn(len(skill_list))
		chk_bool = true
		for sk := range char_skills {
			if sk == skill_list[rnd_int] {
				chk_bool = false
			}
		}
	}

	return skill_list[rnd_int]
}

// Pick a random ego stunt based on the provided map of skills
func genEgoStunt(skill_map map[string]int, ego_stunts []Stunt_Out) Stunt_Out {
	// Ego stunt to return
	var ego_stunt Stunt_Out
	// Working stunt
	var stunt Ego_Stunt
	// Check bool for if stunt is a duplicate
	var dup_stunt bool = true

	for dup_stunt {
		// Skill to choose stunt based off of
		var skill_for_stunt string = calcFromWeights(skill_map)
		// Get a random stunt related to that skill
		stunt = getEgoStuntFromSkill(skill_for_stunt)
		dup_stunt = false
		// Check if character already has stunt
		for _, ex_stunt := range ego_stunts {
			if stunt.Stunt == ex_stunt.Stunt {
				dup_stunt = true
			}
		}
	}

	// Translate to Stunt_Out
	ego_stunt.Stunt = stunt.Stunt
	ego_stunt.Description = stunt.Description
	ego_stunt.Related_Skill = stunt.Associated_Skill
	ego_stunt.Use_Cost = stunt.Use_Cost

	return ego_stunt
}

// Randomly select a valid gear stunt based on character skills
func genGearStunt(skill_map map[string]int, gear_stunts []Stunt_Out) Stunt_Out {
	// Gear stunt to return
	var gear_stunt Stunt_Out
	// Working stunt
	var stunt Gear_Stunt
	// Check bool for if stunt is a duplicate
	var dup_stunt bool = true
	// List of skill names
	var skill_list []string

	for dup_stunt {
		// Get a random stunt related to that skill
		stunt = getGearStunt(skill_list)
		dup_stunt = false
		// Check if character already has stunt
		for _, ex_stunt := range gear_stunts {
			if stunt.Stunt == ex_stunt.Stunt {
				dup_stunt = true
			}
		}
	}

	// Translate to Stunt_Out
	gear_stunt.Stunt = stunt.Stunt
	gear_stunt.Description = stunt.Description
	gear_stunt.Use_Cost = stunt.Use_Cost

	return gear_stunt
}

// Randomly determine ego gender
func genEgoGender(morph Morph_Out, background string) string {
	// Output string
	var ego_gender string
	// Morph sex all lowercase
	var morph_sex_lower string = strings.ToLower(morph.Sex)

	// TODO: Make this user-adjustable
	gender_weights_map := map[string]int{
		"Male":   9960,
		"Female": 9960,
		// Note: As of June 2021, ~.36% of the US population identifies as non-binary
		//       This weight is arbitrarily over double that
		"Non-Binary": 80,
	}

	random_number := rand.Intn(10)
	// 10% chance for ego to be unrelated to morph sex
	if random_number == 0 || morph_sex_lower == "asexual" || morph_sex_lower == "hermaphrodite" || morph_sex_lower == "n/a" {
		if background == "Infolife" {
			gender_weights_map = map[string]int{
				"Male":       3000,
				"Female":     3000,
				"None":       3000,
				"Non-Binary": 1000,
			}
		}
		ego_gender = calcFromWeights(gender_weights_map)

	} else {
		ego_gender = morph.Sex
	}

	return ego_gender
}

// Randomly determine motivations
func genMotivations(bkg_str string, faction_str string) []string {
	// Map of motivations with weights
	var motive_weight_map map[string]int
	// Map of motivaitons with their opposites
	var motive_opposites_map map[string]string
	// Slice of motivation strings to output
	var motivations []string

	motive_weight_map, motive_opposites_map = getMotivations(bkg_str, faction_str)

	for i := 0; i <= rand.Intn(3)+1; i++ {
		// Working new motivation to add to list
		var new_motive string
		// Working bool to check if motivation is valid
		var valid_motive bool = false
		for !valid_motive {
			new_motive = calcFromWeights(motive_weight_map)
			valid_motive = true

			for j := range motivations {
				// Motivation is invalid if it's already in the list or is an opposite to something in the list
				if new_motive == motivations[j] || strings.Contains(motive_opposites_map[motivations[j]], new_motive) {
					valid_motive = false
				}
			}
		}

		motivations = append(motivations, new_motive)
	}

	return motivations
}

// Generate muse skills
func genMuseSkills() map[string]int {
	// Map of skills to output
	var skills_out_map map[string]int = make(map[string]int)

	// 50/50 chance to get standard muse skills
	if rand.Intn(2) == 0 {
		skills_out_map["Infosec"] = rand.Intn(2) + 1
		skills_out_map["Investigate"] = 3 - skills_out_map["Infosec"]
	} else {
		skills_out_map[pickRandSkill(skills_out_map, getSkillsList("muse"))] = 2
		skills_out_map[pickRandSkill(skills_out_map, getSkillsList("muse"))] = 1
	}

	return skills_out_map
}
