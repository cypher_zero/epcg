package api_fate

import (
	"database/sql"
	"log"
	"reflect"
	"strings"

	rand "gitlab.com/cypher_zero/epcg/rnd"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

// FateDB - Database which contains all the Transhumanity's FATE related data
var FateDB *gorm.DB

// ConnectFateDB - Connect to the fate database and migrate relevant tables so they work with gorm
func ConnectFateDB(dsn string) *gorm.DB {
	FateDB, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("FATE DB connection error:", err)
	}

	FateDB = MigrateFateTables(FateDB)

	return FateDB
}

// MigrateFateTables - Migrate the tables in the FATE database to work properly with GORM
func MigrateFateTables(FateDB *gorm.DB) *gorm.DB {
	log.Println("Migrating FATE tables")
	err := FateDB.AutoMigrate(&Team_Role{})
	migrateErr(err, "team_roles")

	err = FateDB.AutoMigrate(&Morph{})
	migrateErr(err, "morphs")

	err = FateDB.AutoMigrate(&Background{})
	migrateErr(err, "backgrounds")

	err = FateDB.AutoMigrate(&Faction{})
	migrateErr(err, "factions")

	err = FateDB.AutoMigrate(&Skill{})
	migrateErr(err, "skills")

	err = FateDB.AutoMigrate(&Morph_Stunt{})
	migrateErr(err, "morph_stunts")

	err = FateDB.AutoMigrate(&Ego_Stunt{})
	migrateErr(err, "ego_stunts")

	err = FateDB.AutoMigrate(&Gear_Stunt{})
	migrateErr(err, "gear_stunts")

	err = FateDB.AutoMigrate(&aspect_noun{})
	migrateErr(err, "aspect_nouns")

	err = FateDB.AutoMigrate(&aspect_verb{})
	migrateErr(err, "aspect_verbs")

	err = FateDB.AutoMigrate(&aspect_adjective{})
	migrateErr(err, "aspect_adjectives")

	err = FateDB.AutoMigrate(&aspect_template{})
	migrateErr(err, "aspect_templates")

	err = FateDB.AutoMigrate(&Motivation{})
	migrateErr(err, "motivations")
	if err != nil {
		log.Fatal("Migrating table 'motivations':", err)
	}

	return FateDB
}

func migrateErr(err error, tableName string) {
	if err != nil {
		log.Fatal("Migrating table '"+tableName+"':", err)
	}
}

// Randomly selects a team role based on weight
func getWeightedTeamRole() Team_Role {
	type role_light struct {
		Name        string
		Role_Weight int
	}

	// Role to return
	var role Team_Role
	// Temporary, lightweight version of the role
	var temp_role role_light
	// Name of role to return
	var role_name string
	// Map of the weights of all the morphs in the role
	role_weights_map := make(map[string]int)

	// Get just the role names and weights
	rows, err := FateDB.Model(&Team_Role{}).Select("name", "role_weight").Rows()
	if err != nil {
		log.Fatal("Failed getting rows:", err)
	}
	defer rows.Close()

	// Generate map of roles with weights
	for rows.Next() {
		err := FateDB.ScanRows(rows, &temp_role)
		if err != nil {
			log.Fatal(err)
		}
		role_weights_map[temp_role.Name] = temp_role.Role_Weight
	}

	role_name = calcFromWeights(role_weights_map)
	FateDB.Where("name = ?", role_name).First(&role)

	return role
}

// Returns the named morph
func getMorph(morph_name string) Morph {
	var morph Morph
	FateDB.Where("morph = ?", morph_name).First(&morph)

	return morph
}

// Returns the named faction
func getFaction(faction_name string) Faction {
	var faction Faction
	FateDB.Where("faction = ?", faction_name).First(&faction)

	return faction
}

// Randomly select a background based on weight
func genBackground() Background {
	// Generated background name
	var background string
	// Working background entry
	var bkg Background
	// Background output
	var bkg_out Background
	// Map of the weights of all backgrounds
	bkg_weight_map := make(map[string]int)

	rows, err := FateDB.Model(&Background{}).Rows()
	if err != nil {
		log.Fatal("Failed getting rows:", err)
	}
	defer rows.Close()

	for rows.Next() {
		err := FateDB.ScanRows(rows, &bkg)
		if err != nil {
			log.Fatal(err)
		}
		bkg_weight_map[bkg.Background] = bkg.Background_Gen_Weight
	}

	background = calcFromWeights(bkg_weight_map)
	FateDB.Where("background = ?", background).First(&bkg_out)

	return bkg_out
}

// Get and return the list of skills from the database
func getSkillsList(morph_type string) []string {
	// Slice of skills from the database to return
	var skill_list []string
	morph_type = strings.ToLower(morph_type)
	morph_type = strings.Trim(morph_type, `\s`)

	if morph_type == "infomorph" {
		FateDB.Model(&Skill{}).Where("ai_valid", true).Pluck("skill", &skill_list)
	} else if morph_type == "muse" {
		FateDB.Model(&Skill{}).Where("muse_valid", true).Pluck("skill", &skill_list)
	} else {
		FateDB.Model(&Skill{}).Pluck("skill", &skill_list)
	}
	return skill_list
}

// Get list of valid stunt for a given morph
func getMorphStuntList(morph Morph) []string {
	// Slice of stunt names from the database to return
	var stunt_list []string

	reflect_morph := reflect.ValueOf(&morph).Elem()
	for i := 0; i < reflect_morph.NumField(); i++ {
		var_name := reflect_morph.Type().Field(i).Name
		var_type := reflect_morph.Type().Field(i).Type

		if strings.Contains(var_name, "Stunt_") && var_type.Name() == "string" {
			var_value := reflect_morph.Field(i).String()
			if var_value != "" {
				stunt_list = append(stunt_list, var_value)
			}
		}
	}

	return stunt_list
}

// Randomly get and return an ego stunt from the database based on the provided skill
func getEgoStuntFromSkill(skill_for_stunt string) Ego_Stunt {
	// List of possible stunts based on skill
	var poss_stunts_list []Ego_Stunt

	poss_stunts, err := FateDB.Model(&Ego_Stunt{}).Where("associated_skill LIKE ?", skill_for_stunt).Rows()
	if err != nil {
		log.Fatal("Error getting ego stunts:", err)
	}

	for poss_stunts.Next() {
		var stunt Ego_Stunt
		err := FateDB.ScanRows(poss_stunts, &stunt)
		if err != nil {
			log.Fatal(err)
		}
		poss_stunts_list = append(poss_stunts_list, stunt)
	}

	if len(poss_stunts_list) == 0 {
		log.Panicln("No stunts for skill (", skill_for_stunt, ") in list:", poss_stunts_list)
	}

	return poss_stunts_list[rand.Intn(len(poss_stunts_list))]
}

// Randomly get and return a gear stunt from the database
func getGearStunt(skills_list []string) Gear_Stunt {
	// List of possible stunts based on skill
	var poss_stunts_list []Gear_Stunt

	skills_list = append(skills_list, "none")

	poss_stunts, err := FateDB.Model(&Gear_Stunt{}).Where("associated_skill LIKE ?", skills_list).Rows()
	if err != nil {
		log.Fatal("Error getting gear stunts:", err)
	}

	for poss_stunts.Next() {
		var stunt Gear_Stunt
		err := FateDB.ScanRows(poss_stunts, &stunt)
		if err != nil {
			log.Fatal(err)
		}
		poss_stunts_list = append(poss_stunts_list, stunt)
	}

	random_number := rand.Intn(len(poss_stunts_list))

	return poss_stunts_list[random_number]
}

// Randomly get and return an aspect adjective
func getAspectAdj(adjQuality string, adjType string, adjSubtype string) string {
	// Aspect adjective entries from the sql database
	var adjEntries *sql.Rows
	// List of valid adjectives
	var adjList []aspect_adjective
	// Working error var
	var err error

	if adjQuality != "any" {
		adjEntries, err = FateDB.Model(&aspect_adjective{}).Where("quality = ?", adjQuality).Rows()
		if err != nil {
			log.Fatal("Error getting aspect adjectives:", err)
		}
	} else {
		adjEntries, err = FateDB.Model(&aspect_adjective{}).Rows()
		if err != nil {
			log.Fatal("Error getting aspect adjectives:", err)
		}
	}

	for adjEntries.Next() {
		// Working adjective database entry
		var adjEntry aspect_adjective
		err := FateDB.ScanRows(adjEntries, &adjEntry)
		if err != nil {
			log.Fatal(err)
		}

		adjList = chkAdjType(adjEntry, adjType, adjSubtype, adjList)
	}

	randomNubmer := rand.Intn(len(adjList))

	return adjList[randomNubmer].Adjective
}

// Checks if the aspect_adjective matches the provided type and subtype strings
func chkAdjType(adjEntry aspect_adjective, adjType string, adjSubtype string, adjList []aspect_adjective) []aspect_adjective {
	// Check if there's a type specified
	if adjType != "" {
		// Check if the entry type matches the specified type
		if adjType == adjEntry.Type || adjEntry.Type == "general" {
			// Check if there's a subtype specified
			if adjSubtype != "" {
				// Check if the subtype matches the specified subtype
				if adjSubtype == adjEntry.Subtype {
					// Set when the type and subtype are both specified and match
					adjList = append(adjList, adjEntry)
				}
			} else {
				// Set when the type is set and matches, but subtype is not specified
				adjList = append(adjList, adjEntry)
			}
		}
	} else {
		// Set if no type is set
		adjList = append(adjList, adjEntry)
	}

	return adjList
}

// Randomly get and return an aspect template
func getTemplate(trouble bool, templType string) string {
	// Template entries from the sql database
	var templEntries *sql.Rows
	// List of valid templates
	var templList []aspect_template
	// Working error var
	var err error

	templEntries, err = FateDB.Model(&aspect_template{}).Where("type = ?", templType).Where("trouble = ?", trouble).Rows()
	if err != nil {
		log.Fatal("Error getting aspect templates:", err)
	}

	for templEntries.Next() {
		// Working template database entry
		var templEntry aspect_template
		err := FateDB.ScanRows(templEntries, &templEntry)
		if err != nil {
			log.Fatal(err)
		}
		templList = append(templList, templEntry)
	}

	randomNubmer := rand.Intn(len(templList))

	return templList[randomNubmer].Template
}

// Get a noun from the database
func getAspectNoun(noun_type string, noun_subtype string, plural bool) string {
	// Noun entries from the database
	var match_entries *sql.Rows
	// List of valid nouns
	var valid_list []aspect_noun
	// Working error var
	var err error

	if noun_subtype != "" {
		match_entries, err = FateDB.Model(&aspect_noun{}).Where("type = ?", noun_type).Where("subtype = ?", noun_subtype).Rows()
		if err != nil {
			log.Fatal("Error getting aspect nouns:", err)
		}
	} else if noun_type != "" {
		match_entries, err = FateDB.Model(&aspect_noun{}).Where("type = ?", noun_type).Rows()
		if err != nil {
			log.Fatal("Error getting aspect nouns:", err)
		}
	} else {
		match_entries, err = FateDB.Model(&aspect_noun{}).Rows()
		if err != nil {
			log.Fatal("Error getting aspect nouns:", err)
		}
	}

	for match_entries.Next() {
		// Working template database entry
		var entry aspect_noun
		err := FateDB.ScanRows(match_entries, &entry)
		if err != nil {
			log.Fatal(err)
		}
		valid_list = append(valid_list, entry)
	}

	randomNubmer := rand.Intn(len(valid_list))

	if plural {
		return valid_list[randomNubmer].Plural
	} else {
		return valid_list[randomNubmer].Noun
	}
}

// Get slice of motivations with data for given background and faction
func getMotivations(bkg_str string, faction_str string) (map[string]int, map[string]string) {
	bkg_str = strings.ToLower(bkg_str)
	bkg_str = strings.ReplaceAll(bkg_str, `-`, `__`)
	bkg_str = strings.ReplaceAll(bkg_str, ` `, `_`)
	bkg_str = bkg_str + "_bkg_weight"
	bkg_str = strings.Title(bkg_str)
	faction_str = strings.ToLower(faction_str)
	faction_str = strings.ReplaceAll(faction_str, `-`, `__`)
	faction_str = strings.ReplaceAll(faction_str, ` `, `_`)
	faction_str = faction_str + "_faction_weight"
	faction_str = strings.Title(faction_str)

	// Motivation entries from the database
	var motivation_entries *sql.Rows
	// Working error var
	var err error
	// Map of motivations with their weights
	var motive_weight_map map[string]int = make(map[string]int)
	// Map of motivation opposites
	var motive_opposites_map map[string]string = make(map[string]string)

	motivation_entries, err = FateDB.Select("Motivation", bkg_str, faction_str, "Opposites").Find(&Motivation{}).Rows()
	if err != nil {
		log.Fatal("Error retreiving motivations:", err)
	}

	for motivation_entries.Next() {
		// Working template database entry
		var entry Motivation
		// Working weight for return map
		var weight int = 0

		err := FateDB.ScanRows(motivation_entries, &entry)
		if err != nil {
			log.Fatal(err)
		}

		// Reflection of all elements in the entry
		var reflect_entry reflect.Value = reflect.ValueOf(&entry).Elem()

		// Loop through the fields to find the ones we care about
		for i := 0; i < reflect_entry.NumField(); i++ {
			var_name := reflect_entry.Type().Field(i).Name
			var_type := reflect_entry.Type().Field(i).Type

			if strings.Contains(var_name, bkg_str) && var_type.Name() == "int" {
				// Get the background weight for the light entry
				weight = weight + reflect_entry.Field(i).Interface().(int)
			} else if strings.Contains(var_name, faction_str) && var_type.Name() == "int" {
				// Get the faction weight for the light entry
				weight = weight + reflect_entry.Field(i).Interface().(int)
			}
		}

		motive_weight_map[entry.Motivation] = weight
		motive_opposites_map[entry.Motivation] = entry.Opposites
	}

	return motive_weight_map, motive_opposites_map
}
