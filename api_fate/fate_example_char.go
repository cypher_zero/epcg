package api_fate

import (
	"encoding/json"
	"log"
)

var exampleCharacter Fate_Char = Fate_Char{
	Name: "Yuri Lambda",
	Ego_Aspects: map[string]string{
		"High Concept": "Utility/Med bot AI",
		"Trouble":      "Open-ended TerraGenesis contract",
		"Freeform 1":   "Polite, yet curt",
		"Freeform 2":   "Prefers machines to people",
	},
	Fate_Points: 1,
	Refresh:     1,
	Gender:      "Male",
	Background:  "Infolife",
	Faction:     "Hypercorp",
	Motivations: []string{"AI Rights", "Open Source", "Personal Development"},
	Skills: map[string]int{
		"Athletics":    2,
		"Civ Rep":      1,
		"Cover":        0,
		"Cred":         0,
		"Deceive":      0,
		"Eye Rep":      0,
		"Fight":        1,
		"Hardware":     4,
		"Infiltrate":   0,
		"Infosec":      3,
		"Investigate":  2,
		"Kinesics":     0,
		"Medicine":     3,
		"Notice":       1,
		"Pilot":        1,
		"Program":      2,
		"Provoke":      0,
		"Rapport":      0,
		"Shoot":        3,
		"Somatics":     1,
		"Survival":     2,
		"Will":         1,
		"Xeno-Contact": 0,
		"X-Risks":      0,
	},
	Ego_Stunts: []Stunt_Out{
		{
			Stunt:         "Weak Spot",
			Related_Skill: "Hardware",
			Description:   "Once per combat, you may declare a devastating attack against a bot or synth opponent",
			Use_Cost:      0,
		},
	},
	Morphs: []Morph_Out{
		{
			Morph:        "Flexbot",
			Type:         "Synthmorph",
			Refresh_Cost: 1,
			Durability:   "Average +1",
			Sex:          "N/A",
			Aspects: []string{
				"Med-unit flexbot module",
				"Utility-unit flexbot module",
			},
			Traits: []string{
				"Essential Synthmorph Traits",
				"Modular Construction",
			},
			Stunts: []Stunt_Out{
				{
					Stunt:         "In-Built Plasma Cutter",
					Related_Skill: "Hardware",
					Description:   "Grants +2 when cutting open doors, bulkheads, etc. using Hardware. Can also be used as a weapon.",
					Use_Cost:      0,
				},
				{
					Stunt:         "First-Aid Augments",
					Related_Skill: "Medicine",
					Description:   "Can reduce a physical consequence by one step immediately after the consequence was gained during combat. Limit one consequence per character; biomorphs only.",
					Use_Cost:      0,
				},
			},
		},
	},
	Muse: Muse_Out{
		Name: "Allie",
		Skills: map[string]int{
			"Kinesics": 2,
			"InfoSec":  1,
		},
	},
	Gear: []Gear{
		{
			Name: "Demolition Charges",
			Info: Stunt_Out{
				Stunt:       "Demolition Charges",
				Description: "You’ve got a supply of explosives, timers, proximity fuses, and the like. Once per session, you can either use a charge to remove an obstacle (Hardware action, difficulty determined by the GM based on the type of obstacle) or make an explosives attack during a conflict. In the latter case, the attack is devastating and unavoidable, affecting every character in the zone where you make the attack.",
				Use_Cost:    0,
			},
		},
	},
}

// ExampleChar returns the example character in JSON format
func ExampleChar() []byte {
	exCharJSON, err := json.MarshalIndent(exampleCharacter, "", "  ")
	if err != nil {
		log.Fatal("Fatal error marshalling JSON: ", err)
	}
	return exCharJSON
}
