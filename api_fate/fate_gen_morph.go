package api_fate

import (
	"strings"

	rand "gitlab.com/cypher_zero/epcg/rnd"
)

// Select a random morph based on given team role and background
func genMorph(role Team_Role, bkg Background, faction Faction) string {
	// The morph name output string
	var morph_name string
	// Maps of morph weights
	r_weights_map := role.morphWeightsMap()
	b_weights_map := bkg.morphWeightsMap()
	f_weights_map := faction.morphWeightsMap()

	// Map of the total weight from all inputs
	sum_weights_map := make(map[string]int)

	for name, val := range r_weights_map {
		sum_weights_map[name] = val + b_weights_map[name] + f_weights_map[name]
	}

	morph_name = calcFromWeights(sum_weights_map)
	// log.Println("Morph:", morph_name)
	morph_name = strings.Replace(morph_name, "_Morph_Weight", "", 1)
	morph_name = strings.Replace(morph_name, "__", "-", 1)
	morph_name = strings.Replace(morph_name, "_", " ", -1)

	return morph_name
}

func genMorphSex(morph Morph) string {
	if morph.Type == "Synthmorph" || morph.Type == "Infomorph" {
		return "N/A"
	} else {
		// The sex of the morph to return
		var sex string
		var sex_weight_map = make(map[string]int)

		sex_weight_map["Male"] = morph.Sex_Weight_Male
		sex_weight_map["Female"] = morph.Sex_Weight_Female
		sex_weight_map["Asexual"] = morph.Sex_Weight_Axexual
		sex_weight_map["Hermaphrodite"] = morph.Sex_Weight_Hermaphrodite

		sex = calcFromWeights(sex_weight_map)

		return sex
	}
}

func getMorphTraits(morph Morph) []string {
	// Output slice of morph traits
	var morphTraits []string = listFromMorphReflect(morph, "Trait_")

	if len(morphTraits) == 0 {
		morphTraits = append(morphTraits, "None")
	}
	return morphTraits
}

func genMorphStunts(morph Morph, fp int) []Stunt_Out {
	// Slice of morph stunts to return
	var morph_stunts []Stunt_Out
	// List of stunts available from the morph
	var morph_stunts_list = getMorphStuntList(morph)
	// Number of morph stunts to select
	var num_stunts int
	// Random number for picking which stunt to select
	var rnd int

	// If morph has no stunts available, return the empty list
	if len(morph_stunts_list) == 0 {
		return morph_stunts
	}

	// Pick random number of stunts to select
	if len(morph_stunts_list) <= fp-1 {
		num_stunts = rand.Intn(len(morph_stunts_list) + 1)
	} else {
		num_stunts = rand.Intn(fp)
	}

	// Pick random stunts from the available list
	for i := 0; i < num_stunts; i++ {
		rnd = rand.Intn(len(morph_stunts_list))
		morph_stunts = append(morph_stunts, Stunt_Out{Stunt: morph_stunts_list[rnd]})

		// Remove the stunt which was just set from the list of stunts
		morph_stunts_list[rnd] = morph_stunts_list[len(morph_stunts_list)-1]
		morph_stunts_list = morph_stunts_list[:len(morph_stunts_list)-1]
	}

	return morph_stunts
}
